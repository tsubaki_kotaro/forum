package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.logic.DateFilteringLogic;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;
	// 作成日時で絞り込んだレコードを取得
	public List<Report> findByReport(String start, String end) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
			// startやendがnullだった場合の処理Logic
			Date startDate = dateFormat.parse(DateFilteringLogic.startDate(start));
			Date endDate = dateFormat.parse(DateFilteringLogic.endDate(end));
			return reportRepository.findByCreatedDateBetweenOrderByCreatedDateDesc(startDate, endDate);
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return reportRepository.findAllByOrderByCreatedDate();

	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// 更新するレコードの取得
	public Report editReport(Integer id) {
		return (Report) reportRepository.findById(id).orElse(null);
	}

	// レコードの削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}
}
