package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;
	// レコード全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAllByOrderByCreatedDate();
	}

	// レコード追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	// 更新するレコードの取得
	public Comment editComment(Integer id) {
		return (Comment) commentRepository.findById(id).orElse(null);
	}

	// レコードの削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
