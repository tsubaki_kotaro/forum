package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;
	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@RequestParam(required = false) String start, @RequestParam(required = false) String end) {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findByReport(start, end);
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// 絞込データオブジェクトを保管
		mav.addObject("start", start);
		mav.addObject("end", end);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集するの投稿データのentityを準備
		Report report = reportService.editReport(id);
		//編集する投稿の内容を保管
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 投稿編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// 投稿データのidをURLで送られたidに変更
		report.setId(id);
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}


	// 投稿削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		// idに紐づけられた投稿を削除
		reportService.deleteReport(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント表示画面
	@GetMapping("/report/{id}")
	public ModelAndView report(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// コメントを全件取得
		List<Comment> contentData = commentService.findAllComment();
		//コメントする投稿内容を取得
		Report report = reportService.editReport(id);
		// 画面遷移先を指定
		mav.setViewName("/report");
		// コメントデータオブジェクトを保管
		mav.addObject("contents", contentData);
		// 投稿データオブジェクトを保管
		mav.addObject("report", report);
		return mav;
	}

	// 新規コメント画面
	@GetMapping("/comment/{id}")
	public ModelAndView newComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Comment comment = new Comment();
		//投稿とコメントを紐づけるための投稿IDをセット
		comment.setReportId(id);
		// 画面遷移先を指定
		mav.setViewName("/comment");
		// 準備した空のentityを保管
		mav.addObject("formModel", comment);
		return mav;
	}

	// コメント投稿処理
	@PostMapping("/add-comment")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment) {
		// 投稿をテーブルに格納
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/report/" + Integer.toString(comment.getReportId()));
	}

	// コメント編集画面
	@GetMapping("/comment-edit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集するの投稿データのentityを準備
		Comment comment = commentService.editComment(id);
		//編集する投稿の内容を保管
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/comment_edit");
		return mav;
	}

	// コメント編集処理
	@PutMapping("/comment-update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		// コメントデータのidをURLで送られたidに変更
		comment.setId(id);
		// 編集したコメントを更新
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/report/" + Integer.toString(comment.getReportId()));
	}

	// コメント削除処理
	@DeleteMapping("/comment-delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id, @ModelAttribute("reportId") Integer reportId) {
		// idに紐づけられた投稿を削除
		commentService.deleteComment(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/report/" + Integer.toString(reportId));
	}
}
