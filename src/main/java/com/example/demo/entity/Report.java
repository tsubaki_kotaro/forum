package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "report")
public class Report {
	@Id
	@Column

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String content;
	@Column(name = "created_date", insertable = false, updatable = false)
	private Date createdDate;
	@Column(name = "updated_date",  insertable = false)
	private Date updatedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@PreUpdate
	public void onPrePersist() {
		Date date = new Date();
		this.updatedDate = date;
		System.out.println(updatedDate);
	}
}