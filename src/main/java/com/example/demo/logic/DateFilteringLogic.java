package com.example.demo.logic;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.ObjectUtils;

public class DateFilteringLogic {
	public static String startDate(String start) {
		if (!ObjectUtils.isEmpty(start)) {
        	start = start.replace("-", "/") + " 00:00:00";
        } else {
        	start = "2020/01/01 00:00:00";
        }

		return start;
	}

	public static String endDate(String end) {
		if (!ObjectUtils.isEmpty(end)) {
        	end = end.replace("-", "/") + " 00:00:00";
        } else {
        	Date nowDate = new Date();
        	SimpleDateFormat nowDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            end = nowDateFormat.format(nowDate);
        }

		return end;
	}
}
